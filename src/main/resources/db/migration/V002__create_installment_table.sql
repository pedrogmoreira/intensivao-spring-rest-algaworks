create table installment (
	id bigint not null auto_increment,
    client_id bigint not null,
    description varchar(100) not null,
    total_value decimal(10, 2) not null,
    installment_quantity tinyint,
    creation_time datetime not null,

    primary key (id)
);

alter table installment add constraint fk_installment_client
foreign key (client_id) references client (id);