package com.phmoreira.awpag.domain.repository;

import com.phmoreira.awpag.domain.model.Installment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The interface Installment repository.
 */
@Repository
public interface InstallmentRepository extends JpaRepository<Installment, Long> {
}
