package com.phmoreira.awpag.domain.repository;

import com.phmoreira.awpag.domain.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The interface Client repository.
 */
@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {
    
    /**
     * Exists by email.
     *
     * @param email the email
     * @return the boolean
     */
    boolean existsByEmail(String email);
    
    /**
     * Find by name containing.
     *
     * @param name the name
     * @return the list
     */
    List<Client> findByNameContaining(String name);
    
}
