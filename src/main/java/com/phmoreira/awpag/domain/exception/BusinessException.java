package com.phmoreira.awpag.domain.exception;

/**
 * The type Business exception.
 */
public class BusinessException extends RuntimeException {
    /**
     * Instantiates a new Business exception.
     *
     * @param message the message
     */
    public BusinessException(String message) {
        super(message);
    }
}
