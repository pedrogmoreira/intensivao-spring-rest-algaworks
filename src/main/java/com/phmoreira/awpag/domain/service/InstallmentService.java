package com.phmoreira.awpag.domain.service;

import com.phmoreira.awpag.domain.exception.BusinessException;
import com.phmoreira.awpag.domain.model.Client;
import com.phmoreira.awpag.domain.model.Installment;
import com.phmoreira.awpag.domain.repository.InstallmentRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;

/**
 * The type Installment service.
 */
@Service
@AllArgsConstructor
public class InstallmentService {
    
    private final InstallmentRepository installmentRepository;
    private final ClientService clientService;
    
    /**
     * Create an installment.
     *
     * @param installment the installment
     * @return the installment
     */
    @Transactional
    public Installment create(Installment installment) {
        if (installment.getId() != null) {
            throw new BusinessException("When registering a new Installment, it should not have an id");
        }
        
        Client client = clientService.find(installment.getClient().getId());
        
        installment.setClient(client);
        installment.setCreationTime(OffsetDateTime.now());
        
        return installmentRepository.save(installment);
    }
}
