package com.phmoreira.awpag.domain.service;

import com.phmoreira.awpag.domain.exception.BusinessException;
import com.phmoreira.awpag.domain.model.Client;
import com.phmoreira.awpag.domain.repository.ClientRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * The type Client service.
 */
@AllArgsConstructor
@Service
public class ClientService {
    
    private final ClientRepository clientRepository;
    
    /**
     * Find a client by id.
     *
     * @param clientId the client id
     * @return the client
     */
    public Client find(Long clientId) {
        return clientRepository.findById(clientId)
                       .orElseThrow(() -> new BusinessException(String.format("Client [id: %d] not found.", clientId)));
    }
    
    /**
     * Create a client.
     *
     * @param client the client
     * @return the client
     */
    @Transactional
    public Client create(Client client) {
        if (clientRepository.existsByEmail(client.getEmail())) {
            throw new BusinessException(String.format("A client with the email '%s' is already registered.", client.getEmail()));
        }
        
        return clientRepository.save(client);
    }
    
    /**
     * Update a client.
     *
     * @param client the client
     * @return the optional
     */
    @Transactional
    public Client update(Client client) {
        if (!clientRepository.existsById(client.getId())) {
            throw new BusinessException("The client provided does not exist.");
        }
        
        return clientRepository.save(client);
    }
    
    /**
     * Delete boolean.
     *
     * @param clientId the client id
     * @return the boolean
     */
    @Transactional
    public void delete(Long clientId) {
        if (!clientRepository.existsById(clientId)) {
            throw new BusinessException("The client provided does not exist.");
        }
        
        clientRepository.deleteById(clientId);
    }
}
