package com.phmoreira.awpag.api.assembler;

import com.phmoreira.awpag.api.model.request.ClientRequestInterface;
import com.phmoreira.awpag.api.model.response.ClientResponseModel;
import com.phmoreira.awpag.domain.model.Client;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;


@AllArgsConstructor
@Component
public class ClientAssembler {
    
    private final ModelMapper modelMapper;
    
    public ClientResponseModel toModel(Client client) {
        return modelMapper.map(client, ClientResponseModel.class);
    }
    
    public List<ClientResponseModel> toCollectionModel(List<Client> installment) {
        return installment.stream()
                       .map(this::toModel)
                       .toList();
    }
    
    public Client toEntity(ClientRequestInterface client) {
        return modelMapper.map(client, Client.class);
    }
    
}
