package com.phmoreira.awpag.api.assembler;

import com.phmoreira.awpag.api.model.request.InstallmentRequestModel;
import com.phmoreira.awpag.api.model.response.InstallmentResponseModel;
import com.phmoreira.awpag.domain.model.Installment;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@AllArgsConstructor
@Component
public class InstallmentAssembler {
    
    private final ModelMapper modelMapper;
    
    public InstallmentResponseModel toModel(Installment installment) {
        return modelMapper.map(installment, InstallmentResponseModel.class);
    }
    
    public List<InstallmentResponseModel> toCollectionModel(List<Installment> installment) {
        return installment.stream()
                       .map(this::toModel)
                       .toList();
    }
    
    public Installment toEntity(InstallmentRequestModel installment) {
        return modelMapper.map(installment, Installment.class);
    }
    
}