package com.phmoreira.awpag.api.model.request;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * The type Installment request model.
 */
@Getter
@Setter
public class InstallmentRequestModel {
    
    @NotBlank
    @Size(max = 100)
    private String description;
    
    @NotNull
    @Positive
    private BigDecimal totalValue;
    
    @NotNull
    @Positive
    @Max(12)
    private Integer installmentQuantity;
    
    @Valid
    @NotNull
    private ClientIdRequestModel client;
    
}
