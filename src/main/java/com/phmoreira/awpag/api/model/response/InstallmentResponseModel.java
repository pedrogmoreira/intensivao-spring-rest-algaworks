package com.phmoreira.awpag.api.model.response;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

/**
 * The type Installment response model.
 */
@Getter
@Setter
public class InstallmentResponseModel {
    
    private Long id;
    private ClientSummaryResponseModel client;
    private  String description;
    private BigDecimal totalValue;
    private Integer installmentQuantity;
    private OffsetDateTime creationTime;
    
}
