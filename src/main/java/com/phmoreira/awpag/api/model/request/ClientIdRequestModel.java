package com.phmoreira.awpag.api.model.request;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

/**
 * The type Client id request model.
 */
@Getter
@Setter
public class ClientIdRequestModel {
    @NotNull
    private Long id;
}
