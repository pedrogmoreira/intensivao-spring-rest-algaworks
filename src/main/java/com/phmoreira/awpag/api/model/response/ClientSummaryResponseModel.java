package com.phmoreira.awpag.api.model.response;

import lombok.Getter;
import lombok.Setter;

/**
 * The type Client summary response model.
 */
@Getter
@Setter
public class ClientSummaryResponseModel {
    
    private Long id;
    private String name;
    
}
