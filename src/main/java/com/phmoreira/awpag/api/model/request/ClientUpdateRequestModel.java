package com.phmoreira.awpag.api.model.request;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;


@Setter
@Getter
public class ClientUpdateRequestModel implements ClientRequestInterface {
    
    private Long id;
    
    @NotBlank
    @Size(max = 60)
    private String name;
    
    @NotBlank
    @Email
    @Size(max = 255)
    private String email;
    
    @NotBlank
    @Size(max = 20)
    private String phone;
    
}
