package com.phmoreira.awpag.api.model.response;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClientResponseModel {
    
    private Long id;
    private String name;
    private String email;
    private String phone;
    
}
