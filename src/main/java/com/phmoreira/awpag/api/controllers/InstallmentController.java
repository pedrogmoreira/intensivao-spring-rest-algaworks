package com.phmoreira.awpag.api.controllers;

import com.phmoreira.awpag.api.assembler.InstallmentAssembler;
import com.phmoreira.awpag.api.model.request.InstallmentRequestModel;
import com.phmoreira.awpag.api.model.response.InstallmentResponseModel;
import com.phmoreira.awpag.domain.model.Installment;
import com.phmoreira.awpag.domain.repository.InstallmentRepository;
import com.phmoreira.awpag.domain.service.InstallmentService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * The type Installment controller.
 */
@AllArgsConstructor
@RestController
@RequestMapping("/installments")
public class InstallmentController {
    
    private final InstallmentRepository installmentRepository;
    private final InstallmentService installmentService;
    private final InstallmentAssembler installmentAssembler;
    
    /**
     * List all installments.
     *
     * @return the list
     */
    @GetMapping
    public List<InstallmentResponseModel> list() {
        List<Installment> installments = installmentRepository.findAll();
        return installmentAssembler.toCollectionModel(installments);
    }
    
    /**
     * Find an installment by id.
     *
     * @param installmentId the installment id
     * @return the response entity
     */
    @GetMapping("/{installmentId}")
    public ResponseEntity<InstallmentResponseModel> find(@PathVariable Long installmentId) {
        return installmentRepository.findById(installmentId)
                       .map(installmentAssembler::toModel)
                       .map(ResponseEntity::ok)
                       .orElse(ResponseEntity.notFound().build());
    }
    
    /**
     * Create an installment.
     *
     * @param installmentRequest the installment request
     * @return the installment
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public InstallmentResponseModel create(@Valid @RequestBody InstallmentRequestModel installmentRequest) {
        Installment installment = installmentAssembler.toEntity(installmentRequest);
        Installment createdInstallment = installmentService.create(installment);
        return installmentAssembler.toModel(createdInstallment);
    }
}
