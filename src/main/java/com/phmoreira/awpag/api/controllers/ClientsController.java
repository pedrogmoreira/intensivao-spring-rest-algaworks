package com.phmoreira.awpag.api.controllers;

import com.phmoreira.awpag.api.assembler.ClientAssembler;
import com.phmoreira.awpag.api.model.request.ClientRequestModel;
import com.phmoreira.awpag.api.model.request.ClientUpdateRequestModel;
import com.phmoreira.awpag.api.model.response.ClientResponseModel;
import com.phmoreira.awpag.domain.model.Client;
import com.phmoreira.awpag.domain.repository.ClientRepository;
import com.phmoreira.awpag.domain.service.ClientService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * The type Clients controller.
 */
@AllArgsConstructor
@RestController
@RequestMapping("/clients")
public class ClientsController {
    
    private  final ClientService clientService;
    private final ClientRepository clientRepository;
    private final ClientAssembler clientAssembler;
    
    /**
     * Lists all clients
     *
     * @return the list
     */
    @GetMapping
    public List<ClientResponseModel> list() {
        List<Client> clients = clientRepository.findAll();
        return clientAssembler.toCollectionModel(clients);
    }
    
    /**
     * Find a client by id.
     *
     * @param clientId the client id
     * @return the response entity
     */
    @GetMapping("/{clientId}")
    public ResponseEntity<ClientResponseModel> find(@PathVariable Long clientId) {
        return clientRepository.findById(clientId)
                       .map(clientAssembler::toModel)
                       .map(ResponseEntity::ok)
                       .orElse(ResponseEntity.notFound().build());
    }
    
    /**
     * Find clients by name.
     *
     * @param clientName the client name
     * @return the response entity
     */
    @GetMapping("/byName/{clientName}")
    public ResponseEntity<List<ClientResponseModel>> findByName(@PathVariable String clientName) {
        List<Client> clients = clientRepository.findByNameContaining(clientName);
        if (clients.size() == 0) {
            return  ResponseEntity.notFound().build();
        }
        
        return ResponseEntity.ok(clientAssembler.toCollectionModel(clients));
    }
    
    /**
     * Create a client.
     *
     * @param clientResquest the client resquest
     * @return the client
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ClientResponseModel create(@Valid @RequestBody ClientRequestModel clientResquest) {
        Client client = clientAssembler.toEntity(clientResquest);
        Client createdClient = clientService.create(client);
        return clientAssembler.toModel(createdClient);
    }
    
    /**
     * Update a client.
     *
     * @param clientId           the client id
     * @param clientRequestModel the client request model
     * @return the response entity
     */
    @PutMapping("/{clientId}")
    public ResponseEntity<ClientResponseModel> update(@PathVariable Long clientId, @Valid @RequestBody ClientUpdateRequestModel clientRequestModel) {
        clientRequestModel.setId(clientId);
        Client client = clientAssembler.toEntity(clientRequestModel);
        Client updatedClient = clientService.update(client);
        return ResponseEntity.ok(clientAssembler.toModel(updatedClient));
    }
    
    /**
     * Delete a client by id.
     *
     * @param clientId the client id
     * @return the response entity
     */
    @DeleteMapping("/{clientId}")
    public ResponseEntity<Void> delete(@PathVariable Long clientId) {
        clientService.delete(clientId);
       
       return ResponseEntity.noContent().build();
    }
    
}