package com.phmoreira.awpag;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The type Awpag api application.
 */
@SpringBootApplication
public class AwpagApiApplication {
    
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        SpringApplication.run(AwpagApiApplication.class, args);
    }
    
}